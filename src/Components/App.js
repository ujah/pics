import React, { Component } from 'react';
import axios from 'axios';
import Searchbar from './SearchBar';
import ImageList from './ImageList';

class App extends Component {

  state={
    images: []
  };

  onSearchSubmit(term){
    console.log(term);
    axios.get(`https://api.unsplash.com/search/photos?query=${term}`, {
      headers: {

        Authorization : 'Client-ID 5b48118f77130379b42dfa6f0d9beb27e8fad9b564615b70bfcc85a24f177b7b'

      }
    }).then((res)=>{
      
      this.setState({
        images: res.data.results
      });
    })

    };

  

  render() { 
    return ( 
      <div className="ui container" style={{marginTop:'10px'}}> 
      <Searchbar onSubmit={e=>{this.onSearchSubmit(e)}}/> Found: {this.state.images.length} images
      <ImageList images={this.state.images}/> 
      </div>
  
     );
  }

}

 
export default App ;