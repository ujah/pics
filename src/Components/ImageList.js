import React from 'react';

function ImageList(props){
    console.log(props.images);
    const images = props.images.map((image)=>{
        if(!image){
            return <div class="ui segment">
            <div class="ui active dimmer">
              <div class="ui text loader">Loading</div>
            </div>
          </div>
        }
        return <div className="images" key={image.id}>
                <img className="pics" alt={image.description} src={image.urls.small} />
              </div>
        
    }) 
    return (
        <div>
            {images}
        </div>
    );
}

export default ImageList;