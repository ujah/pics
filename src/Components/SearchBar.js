import React, { Component } from 'react';

class SearchBar extends Component {
    state = { 
        typing: '',
        term:''
     };

    handleChange (e) {
        this.setState({
            typing: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        // this.setState({
        //     term: e.target.value,
        //     typing:''
        // })

        this.props.onSubmit(this.state.typing)
        }
    
    render() {
        return ( 
            <div className="ui segment">
                <form className="ui form" onSubmit={e=>{this.handleSubmit(e)}}>
                    <div className="field">
                    <label>Image Search</label>
                    <input type="text" value={this.state.typing} onChange={e =>this.handleChange(e)} />
                    </div>
                </form>
            </div>
         );
    };
}
 
export default SearchBar;


